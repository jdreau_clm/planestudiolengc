#include <stdio.h>
#include <conio.h>

int main(){
    int numero = 5;
    int contador = 0;

    while (contador < numero)
    {
        printf("%d- Hola con while\n", contador+1);
        contador++;
    }
    printf("\n\nTermino ciclo while\n\n");

    contador=0;
    do
    {
        printf("%d- Hola con do-while\n", contador+1);
        contador++;
    } while (contador < numero);
    
    printf("\n\nTermino ciclo do-while\n\n");

    /*---------------------------------------------------------*/

    for (contador = 0; contador < numero; contador++)
    {
        printf("%d- Hola con for\n", contador+1);
    }
    printf("\n\nTermino ciclo for\n\n");
}