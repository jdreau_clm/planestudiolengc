#include <stdio.h>
#include <conio.h> // contiene una funcion "sizeof"

int main(){
    printf("Las variables de tipo entero ocupa %d Bytes de memoria\n\n", sizeof(int));
    printf("Las variables de tipo entero corto ocupa %d Bytes de memoria\n\n",sizeof(short int));
    printf("Las variables de tipo entero largo ocupa %d Bytes de memoria\n\n",sizeof(long int));
    printf("Las variables de tipo flotante ocupa %d Bytes de memoria\n\n",sizeof(float));
    printf("Los enteros con decimal ocupan %d Bytes de memoria\n\n",sizeof(double));
    printf("Las enteros largos con decimal ocupan %d Bytes de memoria\n\n",sizeof(long double));
    printf("Las variables de tipo char (Caracteres) ocupan %d Bytes de memoria\n\n",sizeof(char));
    printf("\n\n");

    return 0;
}