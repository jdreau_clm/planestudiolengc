#include <stdio.h>
#include <conio.h>

int main(){
    int variable;
    int arreglo[4] = {1,2,3,4};
    //Para crear una variable string, tengo que crear un arreglo de tipo char
    //no existe las variables tipo string. Al menos hasta ahora de lo que llevo estudiando C
    char arregloChar[] = "Hola Mundo";
    //char caracteres = 'nombre';

    variable = 5;

    printf("variable: %d\n", variable);
    printf("arreglo de caracteres: %s\n", arregloChar);
    //printf("arreglo2 de caracteres: %s\n", caracteres);
    printf("arreglo pos 0: %d\n", arreglo[0]);
    printf("arreglo pos 1: %d\n", arreglo[1]);
    printf("arreglo pos 2: %d\n", arreglo[2]);
    printf("arreglo pos 3: %d\n", arreglo[3]);

    return 0;
}